package cobraEx

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
)

type ArgumentInfo struct {
	Name     string
	Type     string
	Min, Max int
}

func (i ArgumentInfo) String() string {
	text := strings.Builder{}
	text.WriteString(fmt.Sprintf("%s<%s>", i.Name, i.Type))
	return text.String()
}

func AddUsageParameters(cmd *cobra.Command, mandatory []ArgumentInfo, optional []ArgumentInfo, variable *ArgumentInfo) {
	newUsage := strings.Builder{}
	newUsage.WriteString("{{.UseLine}}")
	for _, p := range mandatory {
		newUsage.WriteString(fmt.Sprintf(" %s", p))
	}
	for _, p := range optional {
		newUsage.WriteString(fmt.Sprintf(" [%s]", p))
	}
	if variable != nil {
		newUsage.WriteString(fmt.Sprintf(" {%s}", *variable))
		if variable.Min == 0 {
			if variable.Max > 0 {
				newUsage.WriteString(fmt.Sprintf("(no more than %d)", variable.Max))
			}
		} else {
			if variable.Max > 0 {
				newUsage.WriteString(fmt.Sprintf("(between %d and %d)", variable.Min, variable.Max))
			} else {
				newUsage.WriteString(fmt.Sprintf("(at least %d)", variable.Min))
			}
		}
	}
	usage := strings.Replace(cmd.UsageTemplate(), "{{.UseLine}}", newUsage.String(), -1)
	cmd.SetUsageTemplate(usage)
}
